# JRC chemical risk indicators

## LUCAS soil risk indicator
The script and the data  enable users to derive the LUCAS soil risk pesticide risk indicator, as presented in the [CSS indicator dashboard](https://www.eea.europa.eu/en/european-zero-pollution-dashboards/indicators/ecological-risk-of-pesticides) and in the [associated manuscript](https://setac.onlinelibrary.wiley.com/doi/10.1002/ieam.4917?af=R)

## Description
 
The project is structured in two parts.
1. Derivation of the ecotoxicity reference values for the pesticide monitored in LUCAS soil
2. Calculation of the risk indicators using LUCAS soil monitorng data from IPCHEM

## Status of the project

Part 1: completed
Part 2: work in progress

## Derivation of the ecotoxicity reference values
This part of the project takes as input data collected from three public repositories of ecotoxicity datasets: EFSA's OpenFoodTox, the University of Hertfordshire PPDB, the U.S. EPA ECOTOX.Data retrieved in 2023 are available here as .csv files. 

- PPDB_s_2023.csv is an extraction of NOEC in mg/kg from studies on earthworms and other soil macroorganisms
- OFT_2023.csv is the ecotox module of reference points from EFSA's OpenFoodTox
- EPA_ECOTOX is the extract of in-soil macro-organisms NOEC data from U.S EPA's ECOTOX

The R file "EcotoxDB_2024" generates "DB_LUCAS", a file with derived NOECmin and NOECmedian values.

## Verification and evaluation of ecotoxicity reference values
For a subset of substances identified as risk drivers, ecotixicity records were subject to data verification and evaluation. 
In the verification step the toxicity record as retrieved from the databases was checked to confirm that it was reported correctly from the original EFSA opinions and that it corresponded to the lowest NOEC. 
Data from literature studies that are not reported in EFSA opinions was then subject to study evaluation, using the [CRED soil approach](https://pubmed.ncbi.nlm.nih.gov/38780110/) . 
Details of this step are in the file DB_LUCAS_verification_and_evaluation.xlsx. 
The result of study evaluations are in DB_LUCAS_study_evaluations_CREDsoil.xlsx.

## Calculation of the risk indicators
This step requires the 2018 LUCAS soil pesticides dataset. Access to this dataset on IPCHEM is currently restricted to the European Commission and EU agencies. 

## Authors and acknowledgment
This scientific work was a collaboration between the JRC (Antonio Franco, Diana Vieira, Laure-Alix Clerbaux, Alberto Orgiazzi and Arwyn Jones), the  European Environment Agency (Jeanne Vuaille), the European Food Safety Authority (Edoardo Carnesecchi, Jean-Lou Dorne), University of Vigo (Julia Koeninger), University of Zurich (Maeva Labouyrie) and Wageningen University and Research (Ruud van Dan, Vera Silva).

We aknowledge Gabriele Oprescu for supporting the technical implementation of the project in R

## License
See specific readme


